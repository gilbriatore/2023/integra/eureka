package briatore.pessoa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppPessoa {

	public static void main(String[] args) {
		SpringApplication.run(AppPessoa.class, args);
	}
}
