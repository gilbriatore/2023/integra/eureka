package briatore.pessoa.repository;

import org.springframework.data.repository.CrudRepository;

import briatore.pessoa.domain.Pessoa;

public interface PessoaRepository extends CrudRepository<Pessoa, Long> {
    
}
