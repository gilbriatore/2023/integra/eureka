# Eureka

Exemplos de microserviços que rodam com Eureka Server, Microserviço Gateway e OpenAPI.

## Baixar o projeto...

Fazer o download do zip ou clonar com:

```
git clone https://gitlab.com/gilbriatore/2023/integra/eureka.git
```

## Baixar o IntelliJ IDEA Community Edition...


- [ ] [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/download)


Instalar a IDE, abrir os projetos e executar.