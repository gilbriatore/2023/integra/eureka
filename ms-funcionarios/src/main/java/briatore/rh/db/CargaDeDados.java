package briatore.rh.db;

import briatore.rh.entity.Funcionario;
import briatore.rh.entity.Papel;
import briatore.rh.repository.FuncionarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CargaDeDados {

  private static final Logger log = LoggerFactory.getLogger(CargaDeDados.class);

  @Bean
  CommandLineRunner initDatabase(FuncionarioRepository repository) {

    return args -> {
      log.info("Carregando " + repository.save(new Funcionario("Bilbo Baggins", Papel.Gerente)));
      log.info("Carregando " + repository.save(new Funcionario("Frodo Baggins", Papel.Vendedor)));
    };
  }
}