package briatore.rh.entity;

import briatore.rh.entity.Salario;

public class SalarioGerenteStrategy implements Salario {

  @Override
  public double getValor() {
    return 5000;
  }
  
}
