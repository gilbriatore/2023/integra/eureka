package briatore.rh.entity;

import java.util.HashMap;
import java.util.Map;

public enum Papel {
  Vendedor,
  Gerente;

  static Map<Papel, Salario> strategiesMap = new HashMap<Papel, Salario>(){{
    put(Papel.Gerente, new SalarioGerenteStrategy());
    put(Papel.Vendedor, new SalarioVendedorStrategy());
  }};
  
  static Salario getStrategy(Papel papel){
    return strategiesMap.get(papel);
  }
}


