package briatore.rh.entity;

import briatore.rh.entity.Salario;

public class SalarioVendedorStrategy implements Salario {

  @Override
  public double getValor() {
    return 1000 + (100 * 57);
  }
  
}
