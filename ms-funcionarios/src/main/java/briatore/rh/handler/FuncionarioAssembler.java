package briatore.rh.handler;

import briatore.rh.entity.Funcionario;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class FuncionarioAssembler implements RepresentationModelAssembler<Funcionario, EntityModel<Funcionario>> {
    @Override
    public EntityModel<Funcionario> toModel(Funcionario funcionario) {
        return EntityModel.of(funcionario);
    }

    @Override
    public CollectionModel<EntityModel<Funcionario>> toCollectionModel(Iterable<? extends Funcionario> funcionarios) {
        return RepresentationModelAssembler.super.toCollectionModel(funcionarios);
    }
}
