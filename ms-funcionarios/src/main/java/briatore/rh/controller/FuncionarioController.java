package briatore.rh.controller;

import briatore.rh.entity.Funcionario;
import briatore.rh.exception.FuncionarioNotFoundException;
import briatore.rh.handler.FuncionarioAssembler;
import briatore.rh.repository.FuncionarioRepository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FuncionarioController {

  private final FuncionarioRepository repository;
  private final FuncionarioAssembler assembler;

  FuncionarioController(FuncionarioRepository repository,
                        FuncionarioAssembler assembler) {
    this.repository = repository;
    this.assembler = assembler;
  }

  @GetMapping("/funcionarios/{id}/salario")
  String getSalario(@PathVariable Long id) {
    Funcionario funcionario = getFuncionarioById(id);
    return "Funcionário " + funcionario.getNome()+ 
           ", Salário R$ " + funcionario.getSalario();
  }

  Funcionario getFuncionarioById(Long id){
    return repository.findById(id)
    .orElseThrow(() -> new FuncionarioNotFoundException(id));
  }

  @GetMapping("/funcionarios")
  List<Funcionario> listar() {
    return repository.findAll();
  }
//  CollectionModel<EntityModel<Funcionario>> listar() {
//    return assembler.toCollectionModel(repository.findAll());
//  }

  @PostMapping("/funcionarios")
  Funcionario novoFuncionario(@RequestBody Funcionario novoFuncionario) {
    return repository.save(novoFuncionario);
  }

  @GetMapping("/funcionarios/{id}")
  Funcionario getFuncionario(@PathVariable Long id) {
      return getFuncionarioById(id);
  }

//  EntityModel<Funcionario> getFuncionario(@PathVariable Long id) {
//    return assembler.toModel(getFuncionarioById(id));
//  }

  @PutMapping("/funcionarios/{id}")
  Funcionario atualizarFuncionario(@RequestBody Funcionario novoFuncionario, 
      @PathVariable Long id) {
    
    return repository.findById(id)
      .map(funcionario -> {
        funcionario.setNome(novoFuncionario.getNome());
        funcionario.setPapel(novoFuncionario.getPapel());
        return repository.save(funcionario);
      })
      .orElseGet(() -> {
        novoFuncionario.setId(id);
        return repository.save(novoFuncionario);
      });
  }

  @DeleteMapping("/funcionarios/{id}")
  void excluirFuncionario(@PathVariable Long id) {
    repository.deleteById(id);
  }
}