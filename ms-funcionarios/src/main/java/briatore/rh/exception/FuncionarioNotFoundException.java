package briatore.rh.exception;

public class FuncionarioNotFoundException extends RuntimeException {

  public FuncionarioNotFoundException(Long id) {
    super("Não conseguir encontrar o funcionário " + id);
  }
}